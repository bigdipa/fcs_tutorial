**Lab: Fluorescence correlation** [Gohlke/Digman+TAs]

Lecture will describe case study of processing big image data produced by very
fast acquisition of FCS data and need to optimize the downstream processing.
Narrative discussion of engineering development of the software (FCS Globals),
basic approach/software libraries used to make it fast, resulting performance
speedup, etc.  Students will tour LFD and opportunity to collect data with
uSPIM instrument. Students will perform hands on computational correlation
analysis of FCS datasets. 

**Hands on exercise:**

Students will have hands on guided experience performing image acquisition with
uSPIM. They will collect images of samples labeled to elucidate signaling and
coordination of molecular flow using the tumor suppressor, p53 protein. It
regulates target genes involved in DNA damage mitigation and repair. If cells
become stressed due to DNA damage, p53 will induce genes that trigger cell
cycle arrest and/or apoptosis. Upon activation p53 forms tetramers that are
sufficient to induce p53 transcriptional activity independent of the
concentration of protein in the cell. The localization fluctuation of monomers,
dimers and tetramerization events will allow us to construct molecular
connectivity maps with the fluorescence Diffusion Tensor Imaging (fDTI)
analysis. These spatiotemporal maps may provide an insight to study p53
activation and manipulate them at the appropriate time scale. To map the
spatiotemporal dynamics of p53 tetramerization we used the number and molecular
brightness (N&B) analysis acquired from images.


**References:
**
http://www.lfd.uci.edu/media/tutorials/LFD-Globals-Tutorial-2015.09.20%20-%20Pair%20correlation%20for%20images%20(2D%20maps%20and%20connectivity%20maps).pdf